import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/tutorials",
      name: "posts",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorials-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddTutorial")
    },
    /* TEST FOR IMAGE ADDING*/
    {
      path: "/tutorials/view/:id",
      name: "view",
      component: () => import("./components/ViewTutorial")
    },
    /* CART */
    {
      path: "/marketplace",
      name: "marketplace",
      component: () => import("./components/Marketplace")
    },
    {
      path: "/cart",
      name: "cart",
      component: () => import("./components/Cart")
    },

  ]
});
